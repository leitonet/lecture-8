package Sort;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * Created by alexeyburkun on 3/25/17.
 */
public class Main {
    public static void main(String[] args) {
        ArrayList<User> list = new ArrayList<User>();

        list.add(new User("Vasya", 25));
        list.add(new User("Petya", 30));
        list.add(new User("Ilya", 21));
        list.add(new User("Katya", 28));
        list.add(new User("Ihor", 26));

        for (User user : list) {
            System.out.println(user);
        }

        System.out.println("--- by age");

        Collections.sort(list);
        for (User user : list) {
            System.out.println(user);
        }

        System.out.println("--- by name");
        Collections.sort(list, new AlphabetSorter());
        for (User user : list) {
            System.out.println(user);
        }
    }
}
