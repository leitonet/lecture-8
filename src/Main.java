import javax.swing.text.html.HTMLDocument;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        long tmp = System.currentTimeMillis();
        List<Integer> arrayList = new ArrayList<Integer>();

        for (int i = 0; i < 100_000; i++) {
            arrayList.add(i);
        }

        Iterator<Integer> iterator = arrayList.iterator();
        while (iterator.hasNext()) {
            int item = iterator.next();
            if (item % 2 == 0) {
                System.out.println(item);
            }
        }
        long tmp1 = System.currentTimeMillis();

        System.out.println("Вермя выполнения: " + (tmp1 - tmp));
    }
}
